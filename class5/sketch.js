// MiniX 4: data capture
// Amalie Sofie Due Jensen


let x = 0;
let y = 0;
let spacing = 30;

function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  stroke(255,0,0);
  strokeWeight(2);
  if (random(3) < 1) {
    line(x, y, x+spacing, y+spacing);
    // ellispe(x,y, x+spacing,y+spacing);
  } else {
    line(x, y+spacing, x+spacing, y);
  }
  x+=30;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
