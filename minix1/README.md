## MiniX 1 - Amalie Sofie Due Jensen

URL to my programme: https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix1/

URL to repository (code level): https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix1/sketch.js 




## Screenshot of my programme: 

![](minix1/minix1-screenchot1.jpg)


### My coding experience:

I have a little experience with HTML and JavaScript from gymnasium where I programmed simple websites and games so I do recognise the structure of Atom and some of the functions. I still need a refreshing introduction to the world of programming and coding because it has been some years since i last have used the skills. Therefore i hope this class will help me renew and refine those skills in order to learn more advanced and creative program structures.    


### My programme:

In this minix1 programme i wanted to get used to Atom by trying out simple functions and objects as well as playing with different colors. I have two objects in my programme, a rectangle (_rect()_) and an ellipse (_ellipse()_). I wanted to place the rectangle in the center of my canvas which i solved by using the _rectMode(CENTER)_; syntax as well as the _translate(_) function where I converted the origin of the coordinate system to the center of canvas. In addition I also used different color function tools as for example _fill()_ and _stroke()_ where i played with the colors and outlines of the rectangle and the ellipse. One of my goals for this program was to make it more dynamic by making something move or rotate. Therefore I made the rectangle rotate towards the right while the elipse would move around the rectangle like a planet but the opposite way the rectangle rotated. I wanted to challenge myself by aslo making the ellipse increase and decrease in size while rotating around the rectangle. This was a very great challenge and I spend a long time trying to make it work. In the end I used a _if-else_ function inspired from the p5.js reference library. In this section I just tried writing different variables based on the example from p5js.org until i was satisfied with the movement.


### What I have learned

In the process of making my first minix1 I have rediscovered how frustrating but also rewarding programming is. It is a really slow process and even the most simple task can take hours to execute because of the lack of knowledge to the program structure and the different functions. I have learned that it takes a lot of reseach and testing to figure out how to make something work. Therefore i had a lot of trials and errors throughout the process. It can be very discouraging because it is sometimes difficult to discover what, where and why something dosen't runs accodingly to plan. My original plan was to make a more advanced and creative programme but I discovered that it was very timeconsuming and too challenging compared to my current knowledge of programming. Overall I have learned to navigate in the Atom editor and I have gained a better and renewed understanding of the structure of html and JavaScript.


### Reflections

The difference between reading other peoples code and writing your own is that sometimes it isn't always clear why other people have done certain things. Usually you are familiar with your own code and what the different syntax and functions do. You can learn a lot from reading other peoples work. By studying other peoples code you can learn different approaches and functions that you haven't worked with before thereby gaining inspiration and knowledge to future projects. 

There are both similarities and differences between writing and coding. Both coding and writing has a certain structure that needs to be fullfilled in order for successful comprehension. When you write, words have to be in a special order based on gramma and syntax before it can be translated into something interpretable and meaningful. In the same way coding also has a hierarchy of syntax and structure. If the code isn't written in the correct order the computer can't translate it or run it properly in the browser. Therfore it is very important to know the power of structure of syntax when programming.

Programming is essential in todays society because the boundaries between the digital world and reality are increaingly blurring. It is important to learn coding or maybe just understand the process of programming because digital technologies are dominating the global environment. There are collaborative opportunities in programming because it creates a shared universal langugage or platform that is understood acroos the world. In this way people are able to modify and change programs in collaboration fast and sufficiently from their laptops even with very little knowledge about coding. Coding and programming are important for people to understand and get educated on the current society with its constant and dynamic cultural and political shifts, but it also gives people the power and control over digital tendencies in future.     



### Inspiration 

Most of the help, guidance and inspiration for this programme came from TheCodingTrain on youtube as well as the p5.js website.

https://p5js.org/reference/#/p5/rotate 

https://editor.p5js.org/amcc/sketches/3ZLqytY_4 

https://www.youtube.com/watch?v=o9sgjuh-CBM 







