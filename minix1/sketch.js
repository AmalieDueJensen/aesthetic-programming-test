// MiniX 1: getting started
// Amalie Sofie Due Jensen

let angle = 0;
let dia = 0;
let growAmount = 1;
let grow = true;


function setup() {
// creates a canvas
  createCanvas(600, 400);

//Setting the mode the angle is interpreted. Here it is degrees
  angleMode(DEGREES);

}


function draw() {

//Setting the background color
background(110,20,78);

//making a rectangle in the center of the canvas + making the rect rotate. The center of the rect are a different color than the outline.
push();
translate(300,200);
rotate(angle);
fill(255,131,0);
stroke(0,153,76);
strokeWeight(12);
rectMode(CENTER);
rect(0,0,80);
pop();

//making the ellipse rotate from the center of the canvas the opposite way of the rectangle
translate(300,200);
rotate(-angle*2);

//making a colored ellipse with no outline
fill(255,102,255);
noStroke();
ellipse(120,120,dia);


// sets the rotation angle or speed/rate of rotation of the rect + ellipse
angle = angle + 0.5;


//making the ellipse grow/shrink in size
if (dia > 60) {
  grow = false
}
if (dia < 30) {
  grow = true
}

if (grow == true) {
  dia += growAmount
} else {
  dia -= growAmount
}



}
