## MINIX6 - Revisit the past
Amalie Sofie Due Jensen

**link to runme:** https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix6/

**link to code level:** https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix6/sketch.js 

![](minix6/sol.png)

## Introduction 

This week I chose to rework my minix 1 project because it had more potential for growth and improvement than the rest of my projects. My minix1 was the first programming exercise to get familiar with Atom and the different simple syntaxes. Therefore, I refined the program I made in week 1 but I kept some of the syntaxes I worked with then.  

## My minix1 program

In my minix1 I made a purple background with a rotating orange rectangle with green stroke in the center of the canvas. I also made an ellipse that rotated the opposite way of the rectangle as well as making it grow and shrink in size to make a bouncing effect. 

**Link:** https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/tree/master/minix1 

![](minix6/minix1.png)

## My minix6 program

In this weeks minix project I made a sun in the center of the canvas created by five rotating rectangles and an ellipse in the middle. I made the different rectangles rotate in different directions from each other to make a geometric illusion of sunbeams. The background is light blue. I also made a for loop with light pink ellipses to make the background look cloudy and therefore more dynamic than just a static blue background. I also included a lot of transparency (alpha-parameter) to make the clouds as well as the sunbeams. I did not want to change my first program completely, so I decided to keep working with rotating rectangles and ellipses throughout the canvas. I changed the canvas from the original program because I had only set the canvas to (600*400) and I think it is more aesthetically pleasing when the program covers the entire window. I also played around with different levels of speed to make the sunbeams rotate asynchronously.  


## Reflection on aesthetic programming and digital culture


I would not say my program directly reflect critical making or aesthetic programming because I just modified a previous simple program. I still think it is a fairly simple program because I didn’t want to change the entire aesthetic of my first project. In some elements my minix6 demonstrate aesthetic programming by the way I was able to make something visual and creative by manipulating certain code. Aesthetic programming in general has given me great insight of computational processes in a creative and artsy environment. I really like how we are able to create inspirational and abstract pieces that covers cultural, social and political issues with our own personal touch. Even though my minix6 does not really demonstrate any issues or problematics I still think it resembles something we know from real life. This connection between technologies and human life is what gives aesthetic programming its diverse and flexible character. In these minix projects we are able to make our own worlds by visualizing and materializing issues, themes, and problematics from real life. 

To highlight something from **Aesthetic Programming: A Handbook of Software Studies**, _"Aesthetic programming in this sense is considered as a practice to build things, and makeworlds, but also produce immanent critique drawing upon computer science, art, and cultural theory."_  (Soon Winnie & Cox, Geoff, s. 15)
Aesthetic programming is not only about creating aesthetically pleasing and artsy products. It is also a critical reflection and combination of many different fields like science, engineering, arts and social science. It is about challenging traditional norms and exploring alternative, conceptural and creative possibilities with computational and algorithmic logic. 

This week I only made a sun and some skies but because of the movement and shape from the past project it inspired me to make something that I related to. My project this week is somewhat neutral when it comes to critical reflection and underlying problematics with technologies and power dynamics in society. I did play with some of the natural organic norms when drawing and creating suns by using rectangles instead of lines or other shapes that would look similar to what you normally associate with sunlike shapes. It relates great to a quote from **Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions**, where they emphasizes on the importance of a critical outlook on standard values and norms to reflect on the role and power of technologies in a digital culture, _“…the process of being critical starts by denaturalizing standard assumptions, values, and norms in order to reflect on the position and role of specific technologies within society”_ (Ratto, Matt & Hertz, Garnet ,s. 22)

One aspect where critical making maybe is evident in my minix6 is that I looked on my previous project with new perspective. Today I know a lot more about programming and different and more complex syntaxes, that I did then. Therefore I had more experience with shapes and movements which I used by implementing some of it in my program to elevate it and create, still a simple, but also more advanced than my first project. It is important to look critical and reflect on previous experiences because it gives room for improvement and learning. It makes you realize aspects and hidden opportunities that maybe was not obvious before which is an important perspective in life. In conclusion it is essential to include personal perspective, critical reflection into the material process of working with creative code in a digital culture because it creates new and alternative ways of exploring and experiencing the world, _“The linking of personal investment, critical theory, and material production is what marks critical making as a unique mode of engagement with the world.”_ (Ratto, Matt & Hertz, Garnet, s. 20)


## References: 


Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24 

Ratto, Matt & Hertz, Garnet, “Critical Making and Interdisciplinary Learning: Making as a Bridge between Art, Science, Engineering and Social Interventions” In Bogers, Loes, and Letizia Chiappini, eds. The Critical Makers Reader: (Un)Learning Technology. the Institute of Network Cultures, Amsterdam, 2019, pp. 17-28 (available free online: https://networkcultures.org/blog/publication/the-critical-makers-reader-unlearning-technology/)
