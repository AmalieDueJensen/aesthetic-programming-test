// MiniX 1: getting started
// Amalie Sofie Due Jensen

let angle = 0;

function setup() {
  // creates a canvas
  createCanvas(windowWidth,windowHeight);

  //Setting the mode the angle is interpreted. Here it is degrees
  angleMode(DEGREES);
  // frameRate(30);
}


function draw() {

  //Setting the background color (blue)
  background(102,178,255);

  //setting the random diamter of ellipses in the background - up to 300 pixels
  let randomdiameter = random(300);

  //draw loops of light pink ellipses on the entire canvas
  for (let j = 0; j < 100; j++) {
    fill(255,204,229,30);
    noStroke();
    ellipse(random(windowWidth),random(windowHeight),randomdiameter,randomdiameter);
  }


  //rect1: biggets to smallest making a rectangles in the center of the canvas + making the rects rotate.
  push();
  translate(windowWidth/2,windowHeight/2);
  rotate(angle*1.5);
  fill(255,131,0,170);
  stroke(255,131,0,random(30,90));
  strokeWeight(8);
  rectMode(CENTER);
  rect(0,0,410);
  pop();

  //rect2
  push();
  translate(windowWidth/2,windowHeight/2);
  rotate(-angle*1.5);
  fill(245,175,14,190);
  // noStroke();
  stroke(245,175,14,random(30,90));
  strokeWeight(5);
  rectMode(CENTER);
  rect(0,0,400);
  pop();

  //rect3
  push();
  translate(windowWidth/2,windowHeight/2);
  rotate(angle*0.5);
  fill(255,131,0,210);
  // noStroke();
  stroke(255,131,0,random(30,90));
  strokeWeight(5);
  rectMode(CENTER);
  rect(0,0,380);
  pop();

  //rect4
  push();
  translate(windowWidth/2,windowHeight/2);
  rotate(-angle*0.5);
  fill(233,127,6,200);
  // noStroke();
  stroke(233,127,6,random(30,90));
  strokeWeight(8);
  rectMode(CENTER);
  rect(0,0,340);
  pop();

  //rect5
  push();
  translate(windowWidth/2,windowHeight/2);
  rotate(angle);
  fill(255,140,0,200);
  // noStroke();
  stroke(255,140,0,random(30,90));
  strokeWeight(8);
  rectMode(CENTER);
  rect(0,0,290);
  pop();

  //center ellipse
  push();
  translate(windowWidth/2,windowHeight/2);
  rotate(angle*0.5);
  fill(245,175,14,180);
  noStroke();
  // stroke(245,175,14,random(70,200));
  // strokeWeight(12);
  ellipseMode(CENTER);
  ellipse(0,0,190);
  pop();


  // sets the rotation angle or speed/rate of rotation of the rect + ellipse
  angle = angle + 0.5;

}
