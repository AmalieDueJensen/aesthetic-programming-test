// MiniX 2: Emojis
// Amalie Sofie Due Jensen

let counter = 0;
let movement = 0;
let direction = 1;


function setup() {
createCanvas(windowWidth, windowHeight);
frameRate(46);
}


function draw() {
background(230,230,250);


//setting the movement and direction of emoji 1
movement = movement + direction

if(movement < -40 || movement > 25){
  //direction = direction * -1
  direction *= -1
}


//EMOJI 1
//spikes
fill(87,138,139,245);
stroke(87,138,139,220);
triangle(306,206+movement,270,250+movement,248,170+movement);
triangle(420,204+movement,458,237+movement,482,170+movement);
triangle(278,334+movement,324,362+movement,249,388+movement);
triangle(456,330+movement,420,362+movement,481,388+movement);


//body emoji 1
noStroke();
fill(95,158,160);
stroke(87,138,139);
strokeWeight(6);
ellipse(367,285+movement,200,200);


//mouth
fill(255);
stroke(70,118,119);
strokeWeight(10);
ellipse(367,305+movement,100,100);


//mouth cover
fill(95,158,160);
noStroke();
ellipse(367,280+movement,120,110);


//nose
fill(218,112,214);
stroke(189,101,186,150);
strokeWeight(4);
ellipse(365,290+movement,50,30);
//fill(255,240,245);
fill(245,204,243);
noStroke();
ellipse(372,286+movement,20,10);


//eyeballs
fill(255);
stroke(243);
strokeWeight(3);

//left eyeball
ellipse(315,255+movement,35,37);

//middle eyeball
ellipse(365,235+movement,43,45);

//right eyeball
ellipse(415,255+movement,35,37);


//pubils
fill(0);
stroke(106,90,205,120);
strokeWeight(4);

//left
ellipse(322,250+movement,18,20);

//middle
ellipse(365,240+movement,22,23);

//right
ellipse(408,260+movement,20,22);



//SHADOWS EMOJI 1
//big shadow
push();
fill(148,171,201);
stroke(176,196,222,150);
strokeWeight(12);
ellipse(366,470,120+movement,50);
pop();

//small shadow
fill(119,147,182);
noStroke();
ellipse(366,469,60+movement,20,230);



//ORANGE CANVAS
fill(244,164,96);
strokeWeight(10);


//flashing canvas stroke
if(counter>30){
  counter=0;
}

if(counter<=15){
  stroke(238,130,238);
}

if(counter>15 && counter <= 30) {
  stroke(255,105,180);
}


//counter = counter + 1
counter++


//canvas size
rect(750,100,400,400,100);


//text: MASK UP!
textSize(48);
fill(106,90,205);
noStroke();
text('MASK UP!',835,80);
textStyle(BOLD);


//MASK
//mask strings
fill(244,164,96);
stroke(255);
strokeWeight(5);
ellipse(870,290,78,70);
ellipse(1019,290,78,70);


//mask body
fill(72,209,204);
stroke(255);
strokeWeight(7);
rect(870,250,150,80,3);


//mask lines
stroke(64,186,182);
line(887,272,1001,272);
line(887,292,1001,292);
line(887,311,1001,311);


//MASK SHADOWS
//big mask shadow
fill(223,155,96);
stroke(223,155,96,70);
strokeWeight(5);
rect(875,360,140,33,10);


//small mask shadow
fill(186,132,84,70);
rect(885,367,120,20);

}
