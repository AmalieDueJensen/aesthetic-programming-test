## MINIX2 - EMOJI - Amalie Sofie Due Jensen

My program: https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix2/

Code repository: https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix2/sketch.js 


![](minix2/screenshot_1_emoji.png)


## My program

In this program I have made two emojis one emoticon and one emoji. I have worked with geometric shapes (ellipses,rectangles, lines and triangles) to create my emojis. The one on the left is a floating happy little green-ish monster with three purple eyes and four spikes. I have focused on using traditional emoticon shapes such as ellipses as well as clean and minimalistic details and colors. Even though I chose to make a monster-emoji with some atypical features it still has some universal or even standardized features that resembles characteristics from real life people. (smile, round eyes, round face) 
My second emoji (right) resembles a "corona" mask. I chose to make this because I wanted to make something that was relatable as well as very essential in society today. The mask is surrounded by a flashing pink frame and I have written "MASK UP!” above the frame. The background is set to a light purple color. 
 

## Syntax and functions

In this project I have worked with a lot of different functions and syntax. I have concentrated on small details such as transparency (Alpha-parameter (line 105)), different colors as well as shadow illusions made with ellipses in similar color-tones (line 101-113 or line 175-185). I have also included conditional structures (if-statements (see line 19-24 or line 122-132)) and defined variables with the let function to make my left emoji appear like it was floating up and down. I used the if statements to make my emoji move and to make my right canvas flash. In conclusion I have mainly used geometric structures to make this program in different shapes, colors and sizes. 


## A learning process

In this program I chose to introduce conditional structures (if-statements). In the beginning it was quite difficult to figure out how to organize the statements, and I still don't understand the structure completely. My program has a lot of small details that needed to be set up correctly for the program to run properly and accordingly to expectations. This was one of the reasons why I had difficulties making the conditional statements work. In the end with some help from The Coding Train on YouTube and Google I made it work. When I start a new programming project, I always get reminded of the continuing process of trial and errors. I tend to set my ambitions high in the beginning which results in a very confusing process of trying to make things work, and usually I need to lower those expectations by producing something less advanced. It can be very frustrating at times when things don't run as planned but, in the end,, it is especially rewarding when you see the final picture.  


## Reflection 

Before a made my final emojis I could not figure out what kind of emoji I wanted to create. I started the process by researching on google as well on my phone in the hope of getting an emoji revelation. I also asked my 10-year-old cousin if she had some inspiring and creative ideas to an emoji. Thereby I could get a more youthful and innocent perspective on how the "perfect" emoji should look. 
 
Here are some of the drawings that she made to guide me. 
 
•	Pictures of my cousins drawings. 

![](minix2/Smilla_-_emoji_1.PNG)
![](minix2/Smilla_emoji_2.PNG)

 
In the beginning I tried recreating one of her ideas just to get me started whereas I used that figure as a canvas to my final creation. Thereafter I just randomly tried different things to make the emoji appear more detailed. 


   (my fist emoji creation)
   
![](minix2/emoji_canvas.PNG)



## Political and cultural perspective

One important aspect of emojis and emoticons is that they should be understandable and relatable to the majority as well as being inclusive to diverse people and cultures. Emojis universalism and standardization tend to be more exclusive than the contrary because of very stereotypical and biased representations. 
Emojis is supposed to be funny and creative as well as being an abstraction of people’s emotions. 
It is a difficult challenge to create emojis that embrace everybody’s personality, cultures, and ideologies without them being oversimplified and discriminating to some. This inequality in emojis is one of the reasons why I tried avoiding recognizable human features in my emoji. On the other hand, I still chose to use very traditional shapes (ellipses) which is very universal to the emojis that already exists. In future I should maybe have challenged the circular geometric conventions by using more unconventional shapes and structures. In the end, I have included some features that are not represented in traditional emojis by the fact I have incorporated funky colors and alien-like elements.
My “corona” mask emoji is relevant today, but it will maybe also have some negative political reactions. Not everyone is advocates or agrees with the health-related corona-restrictions that the mask represents. Therefore, the mask-emoji can still be triggering to some.  
I do not believe that there will ever be a “perfect” emoji, but it is important to continue challenging, modifying, and redesigning the traditional and universal norms in hope of someday achieving a new and more inclusive standard.   


