// MiniX 3: Throbber
// Amalie Sofie Due Jensen


var button;
var img;

//the width and the height of the loading squares
var squareWidth = 25;
var squareHeight = 20;

//the distance between the vertical square rows
var squareDistance = 30;

//boolean statement: button
var buttonState = false;

//start time of the countdown timer - number calculated from 21-02-2021
var interval = 10803180
var timer;


function preload() {

  //load image on home page of Orange Scence
  img = loadImage('Orangescene.jpg')
}


function setup() {
  createCanvas(windowWidth, windowHeight);
  //set background to black
  background(0);
  //center the background image
  imageMode(CENTER);
  frameRate(6);
  setInterval(timer, 1000);
  textAlign(CENTER)
  textSize(50)
  textStyle(BOLD)


  //Orange Button on the front page

  let col = color(255,140,0,250);

  button = createButton('Click here to start festival countdown');
  button.style('background-color', col);
  button.style('fontSize','18px');
  button.size(330,42);

  //when the button is pressed the background changes to black
  button.mousePressed(changeBG);

  }

//the background changes to black and the button is hidden. buttonState set to True
function changeBG() {
  button.hide();
  buttonState = true;
  }

//creates countdown
function timer() {
  if (interval > 0){
    interval--;
  }
  }


function draw() {

// when buttonstate == true - the background is set to black, the countdown text gets a white outline.
  if (buttonState){
    background(0);
    fill(0)
    stroke(255)
    strokeWeight(5);
    text(interval + " SECONDS", windowWidth/2,(windowHeight/2)+150);


//the throbber
//set stroke to black
    stroke(0);
    strokeWeight(3);

//4 squares vertical, orange color, random transparency, placed around the center
  for (var i = 0; i < 4; i++) {
    fill(255, 100, 0, random(i * 50));
    rect((windowWidth/2)-(squareDistance*5), ((windowHeight/2)-22*i), squareWidth,squareHeight,4);
  }

//5 squares vertical, orange color, random transparency,
  for (var i = 0; i < 5; i++) {
    fill(255, 100, 0, random(i * 70));
    rect((windowWidth/2) - (squareDistance*4), ((windowHeight/2)-22*i)-10, squareWidth,squareHeight,4);
  }
//11 squares vertical, orange color, random transparency,
  for (var i = 0; i < 12; i++) {
    fill(255, 100, 0, random(i * 60));
    rect((windowWidth/2) - (squareDistance*3), ((windowHeight/2)-22*i)+(squareHeight*2.5), squareWidth,squareHeight,4);
  }

//7 squares vertical, orange color, random transparency,
  for (var i = 0; i < 7; i++) {
    fill(255, 100, 0, random(i * 50));
    rect((windowWidth/2) - (squareDistance*2), ((windowHeight/2)-22*i)+squareHeight, squareWidth,squareHeight,4);
  }
//5 squares vertical, orange color, random transparency,
  for (var i = 0; i < 5; i++) {
    fill(255, 100, 0, random(i * 70));
    rect((windowWidth/2) - squareDistance, ((windowHeight/2)-22*i)-10, squareWidth,squareHeight,4);
  }
//3 squares vertical, orange color, random transparency,
  for (var i = 0; i < 3; i++) {
    fill(255, 100, 0, random(i * 80));
    rect((windowWidth/2), ((windowHeight/2)-22*i)-40, squareWidth,squareHeight,4);
  }
//4 squares vertical, orange color, random transparency,
  for (var i = 0; i < 4; i++) {
    fill(255, 100, 0, random(i * 80));
    rect((windowWidth/2) + squareDistance, ((windowHeight/2)-22*i)-30, squareWidth,squareHeight,4);
  }
//10 squares vertical, orange color, random transparency,
  for (var i = 0; i < 10; i++) {
    fill(255, 100, 0, random(i * 50));
    rect((windowWidth/2) + (squareDistance*2), ((windowHeight/2)-22*i)+40, squareWidth,squareHeight,4);
  }
//6 squares vertical, orange color, random transparency,
  for (var i = 0; i < 6; i++) {
    fill(255, 100, 0, random(i * 70));
    rect((windowWidth/2) + (squareDistance*3), ((windowHeight/2)-22*i), squareWidth,squareHeight,4);
  }
// 4 squares vertical, orange color, random transparency,
  for (var i = 0; i < 4; i++) {
    fill(255, 100, 0, random(i * 55));
    rect((windowWidth/2) + (squareDistance*4), ((windowHeight/2)-22*i)+10, squareWidth,squareHeight,4);
  }

//when the button is not clicked the canvas alters in relation to the size of the browser-window + button position
} else {
  image(img, windowWidth/2,windowHeight/2,windowWidth,windowHeight);
  button.position((windowWidth/2)-165,(windowHeight/2)+190);
}
}
