My Program: https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix3/ 

Code level: https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix3/sketch.js 


## My program

![](minix3/frontpage.PNG)

![](minix3/throbber.PNG)

In the beginning I did not know what kind of throbber I wanted to make. My first idea was to make a simple and minimalistic throbber, like the one we worked with in class. I started making simple changes to the program, by changing the background color and the number of ellipses displayed in the canvas. Thereafter I tried googling different throbbers and loading elements to get inspiration for my final project. I ended with throbber that resembles Roskilde Festival logo. (see inspiration picture) I used orange, black and white as my main colors inspired by the official Roskilde Festival website. (https://www.roskilde-festival.dk/da/)

![](minix3/Billede1.jpg)

It was by accident that I ended up making a Roskilde Festival theme in my program because I randomly played around with the colors in the beginning whereas I really liked the orange color on a black canvas. Under my throbber I chose to incorporate a countdown in seconds as a funny and satirical element. The corona situation has caused a lot of waiting games to larger social and cultural events like festivals and concerts and in my program, I have literaly in black and white displayed what feels like an endless waiting game. 


## The technical aspect 


On new aspect I have not used previously is the loadImage function which I used to make my front page with a background-picture of Orange Scene from Roskilde Festival. (line 26) 
In this program I have explored and experienced with _for loops_ and _Boolean statement_ as well as making my own variables.
I used a _Boolean statement_ to make my throbber and the countdown appear after pushing the button on my front page. I used a _Boolean statement_ because I didn’t my throbber and countdown to appear at the same time as my front page. By setting my _buttonState_ to false in my variable and thereafter setting it to equal to true in my _Boolean statement_ (line 74) as well in my changed background function (line 60) it controls that it will only appear on a clean black canvas after pushing the button on the front page. Otherwise, the program will stay on the front page, resizing compared to the size of the webbrowser.

I used _for loops_ to make my throbber with 10 vertical rows of rectangles that are displayed with random levels of transparency, which almost resembles the movement of an equalizer. 

![](minix3/Billede2.png)
 
This is an example of one of my _for loops_. It will make a vertical row of 0-4 rectangles. The boxes is in orange and random transparency. The higher the number in the alpha parameter this more apparent are the boxes. The last line relates to make the throbber in the middle of the canvas. Here I used my variables _squareDistance _= 30 and _squareHeigh_t = 20 to control the position of each row compared to the center of the canvas. 


## How is time being constructed in computation?

Time in computation does not operate the same way as we know it from human life. In real life time is something that continuously works in a linear and uncontrollable manner. In computation the programmer can play god by manipulating time in a desired way. Time can be stopped/started, reversed/looped, slowed/speeded up. Time is controllable and it can be jumped and looped to infinity. It is a very paradoxical concept that consists of many layers of multiple timelines. This gives the programmer a power that does not exist in the real world. In my program I controlled/manipulated time by making my throbber appear in a constant rhythm between different colors and transparency levels as well I maded time count down instead of going forward. 


## Reflection 

Throbbers are important in digital culture and in computation. They are a visual symbol of the computer processing and loading multiple micro-temporal processes. I do not usually notice throbbers because they have become an automatization of my everyday life. Only if the program is exceptionally long about loading, I start to notice them. After learning a little about computation and programming I have become aware of the element of disguise that the throbber also represents. There are a lot of underlying processes of transferring data and information which is not apparent to everyone. I think it is important to be more aware of these hidden information transfers when learning about software and computational structures because they creates new ways of experiencing time. The throbber is a cultural and digital icon that conceil information to the ignorant user and offers a space for speculation and discussion of how time is being deconstructed and organised creating new and alternative temporal experiences and perceptions. This complexity of digital culture and information transferring have to be critically discussed and adressed for future development computational systems and networks. 


## Inspiration 

https://www.roskilde-festival.dk/da/

https://editor.p5js.org/asadsexyimp/sketches/bgwomRCba 


