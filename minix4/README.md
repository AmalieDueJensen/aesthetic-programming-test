## MINIX4 - DATA CAPTURE 

**Amalie Sofie Due Jensen**

**My program:** https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix4/ 

**Code level:** https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix4/sketch.js 


![](minix4/screen1.png)

![](minix4/screen2.png)




## Introduction

After watching the documentary with Shoshana Zuboff on surveillance capitalism it really got me thinking about the notion of privacy or rather the illusion of privacy when dealing with technologies and online platforms. The documentary was really interesting, and it gave great critical insight of the influence of data extraction and transferring of personal information which is a process that rarely is transparent and evident to the people using the platforms. People are not always aware of the consequences of their behaviors on online environments and these technologies and systems are able to extract and predict extremely personal and detailed information about their emotions and behaviors. It is a scary reality based on monitisation and surveilance of the users actions. One part of the documentary that was particular interesting to me was the part where Facebook claims to be building “_a privacy focused social platform_" (37:20) when in reality it is very far from the truth. These shadow processes and illusions of privacy are the main themes I have concentrated on in this minix.


## The Illusion of Privacy 

Privacy is a very important and essential element of life to many people. But what happens to privacy when moving from reality to online social platforms? Are we aware of the contradicting processes going on with or without your consents and knowledge to the underlying data extraction and transfers? The lack of transparency with data capture on online socail platforms are frightening. This piece therefore highlights some of those problematic consequences and illusions of privacy in an online environment. Therefore, the title of this work is _The Illusion of Privacy_. The frontpage looks somewhat neutral with a blue background (kind of the same color as Facebooks logo). There is a grey/beige canvas in the middle of the screen which resembles a computer screen with a web cam indicator on the top. If you allow the browser to use your web cam then your face will appear inside the computer screen frame. In the screen there are a button that says, “click here for private mode” and when you click the button the background changes to black. There are also displayed “privacy loading” in a blinking greenish color on the left side of the computer frame. The words "privacy", "loading", "error", "data", "backup" flashes continuesly in random places in the background (the same green color as the "privacy loading" sign).   


## The Technical aspect

In this minix I chose to incorporate some of the different elements we worked with in class (web cam, clmtracker, MousePressed). I wanted to get familiar with these new tools in programming and it has been a fun learning experience trying to work with these new coding features. It has also been a great challenge for example setting up the html index the right way according to the new clm tracker library and getting the web cam to work in my program. After some practice and help from the instructors I got it to work correctly. I have also used an array in this minix to display random flashing words in the background (line 5 + 94-104). Originally I also wanted to try make my button a little more detailed but I used a lot of time getting used to the new face tracker library so therefore I chose to keep it simple and as it is (button: line 39-49). In this minix I have also worked with conditional statements to make my background change when the button is pressed. (line 94-168) I used the face tracker to make a black privacy bar (line 141) that covers and follows the location of the eyes. I also chose to make the privacy bar switch between modes of transparency so that sometimes you can still see the eyes through the bar (line 139). I did this to play with the idea of illusion of privacy because even when you think you are private, reality can be totally different and corporations can still monitor your behavior and extract data and information about you. 


## Reflection on data capture 

This week’s work with data captures really highlighted some of the critical and problematic consequences when dealing with modern technologies and online social platforms. Things are rarely what they appear to be and there are a lot of shadow processes that users aren't aware of. Data capture is very powerful tool and it is sometimes used in manipulative and controlling ways by big corporations. My minix should be satirical in a way it displays the illusion of privacy, because it tries to make fun of online culture and the belief that privacy is also something that exists on online platforms like facebook or Instagram. It is really interesting how a real-life concepts like privacy is displayed online like private accounts and limited access as well as privacy policies. After watching the documentary with Shoshana Zuboff on surveillance capitalism it certainly put the concept of privacy into critical perspective. It is really a misconception to think that you can control your personal information and keep it hidden form big corporations like google or facebook. They are able to manipulate and predict our thoughts and behavivoirs based on our personal data. The future of privacy is unclear and the expansion and merging of technologies and online systems are the main contributors to that uncertainty. Surveillance capitalism is inescapable. Even when people think they have “nothing to hide” the control and manipulative process of data capture should be more transparent and apparent to the people using the systems. We are not just digital exhaust to be manipulated with. My work is not a manifestation to make people stop using monitoring systems like Facebook or Google, because that would be an unrealistic goal. My program should be a critical point of view that puts data capture and the illusion of privacy into perspective to make people consider the power and importance of their behaviors in an online reality. 


## Inspiration

Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw. 

https://editor.p5js.org/aferriss/sketches/S1Fn7pHbX 



