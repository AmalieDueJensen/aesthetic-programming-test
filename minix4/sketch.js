// MiniX 4: data capture
// Amalie Sofie Due Jensen

let myfont;
let privacy = ["Privacy","Loading","Error","data","Backup"];
let index = 0;
let i = 0;
let button1;
let buttonState = false;
let ctracker;
let capture;
let buttonx;
let buttony;


function preload() {
  myfont = loadFont('DotGothic16-Regular.ttf')
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  //blue background
  background(0,102,204);


//web cam capture
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();


//setup face tracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);


//button position
  buttonx = windowWidth/2;
  buttony = windowHeight/2;

  //create private mode button
  //button1
  let col = color(255,0,0,170);
  button1 = createButton('Click for Private Mode');
  button1.position(buttonx-120,buttony+50,90,70);
  button1.style('fontSize','25px');
  button1.style('background-color', col);
  button1.mousePressed(changeBG);
}


function changeBG() {
  button1.hide();
  buttonState = true;
}


function draw() {
  //web cam canvas in the center of the canvas

  //computer frame
  push();
  translate(windowWidth/2,windowHeight/2);
  rectMode(CENTER);
  fill(226,226,217);
  noStroke();
  rect(0,0, 690,550,5);


  //webcam setup
  imageMode(CENTER);
  image(capture,0,0,640,480);


  //computer webcam linse
  fill(0);
  stroke(32);
  strokeWeight(2);
  ellipse(0,-257,23,23);


  //computer webcam indicator
  fill(255);
  noStroke();
  ellipse(25,-257, 8,8);
  pop();

// - black background
  if (buttonState){
    background(0);


//Random flashing words - black background
    push();
    index++;
    if(index >= privacy.length){
      index = 0;
    }

    for(let i = 0; i< privacy.length; i++){
      text(privacy[i], random(width), random(height));
    }
    pop();


    //computer frame
    push();
    translate(windowWidth/2,windowHeight/2);
    rectMode(CENTER);
    fill(226,226,217);
    noStroke();
    rect(0,0, 690,550,5);

    //webcam setup
    imageMode(CENTER);
    image(capture,0,0,640,480);

    //computer webcam linse
    fill(0);
    stroke(32);
    strokeWeight(2);
    ellipse(0,-257,23,23);
    fill(32);
    noStroke();
    ellipse(0,-257,17,17);

    //computer webcam indicator
    fill(255);
    noStroke();
    ellipse(25,-257, 8,8);
    pop();


//position of the face tracker + privacy bar
    let positions = ctracker.getCurrentPosition();

    if (positions.length>0) {
        fill(0,random(170, 250));
        noStroke();
        rect(positions[27][0]+270, positions[32][1]+45, 180,45,2);

        // for(let i=0; i< positions.length; i++) {
        //     noStroke();
        //     fill(255,0,0);
        //     ellipse(positions[i][0]+320, positions[i][1]+65, 5,5);
        //   }
  }


//flashing "privacy loading" -  top left corner (between black and green)
    i = i + 1

    if (i % 15 === 0) {

      //random code in the background
      fill(0);
      textFont(myfont);
      textSize(30);
      text('Privacy Loading...', 20,50);

    } else {

      //random code in the background
      fill(153,255,51);
      textFont(myfont);
      textSize(30);
      text('Privacy Loading...', 20,50);

    }
  }
}
