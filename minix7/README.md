## MINIX7 - OBJECT ABSTRACTION
Amalie Sofie Due Jensen

**RunMe:** https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix7/

**Code Level:**

- Sketch.js: https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix7/sketch.js

- Flower.js: https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix7/Flower.js 

- Corona.js: https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix7/Corona.js 

## Introduction

In this week minix project we worked with object abstraction and object-oriented programming by creating a simple game. I did not have a goal in mind in the beginning, so I started the process by playing around with the Tofu game sample code. In class we got familiar with the different syntax and we got introduced to a class-based approach when dealing with objects in programming. I think this weeks minix was very challenging, so I created my game with great inspiration and modification of the syntax from the Tofu Game. I think one of the reasons I find this project more challenging than previous minix programs is because games are a lot more complex and interactive than just aesthetic digital art pieces. There are a lot of small components and properties to keep track of like, objects, score, interactivity and game flow and narrative even in the simplest games. This complexity in programming games made it a little hard for me to wrap my head around and figure out where to start. Therefore, I chose to build my game inspired from the Tofu Game. 


## My Game: Flower Power

I decided to call my game Flower Power because I think I was a fitting name that somewhat represented the narrative of my game. Flower Power is a game consisting of a front page with the game rules. You gain points by moving a flower basket from left to right catching falling green flowers from the sky without catching coronavirus. If you catch coronavirus your score goes down with -1 point and if you get to 15 points by catching the flowers you win. In addition of you miss 10 flowers you will lose the game. The background is in a light purple color and the flowers fall in different sizes and speed levels to make the game a little more difficult to complete. 

![](minix7/startPage.png)

![](minix7/game.png)

![](minix7/win.png)

![](minix7/gameover.png)


## The Technical aspect 

I have used two class properties in my game (Flower.js and Corona.js) to make the falling flowers as well as the corona particles. Both the flowers and the corona particles have some of the same behavioral patterns because they are falling from random places in random speed levels. The difference between the two classes are that the Flower consists of ellipses and changes between different sizes (line 8 +22-33, Flower.js) while the Corona element is an image set to 50 pixels. (line 7, Corona.js) I also changed the minimum number of each object to make more flowers than corona particles appear on the screen.(line4 + +7, sketch.js)  I used the syntax from the Tofu game to guide me and figure out, how to make the flowers fall from the top instead from the right. I changed the syntax in the move section (Flower.js) by writing this.pos.Y instead of this.pos.x (line 14, Flower.js) 
I also chose to make my game in vibrant retro colors because I really like the cartoonish and nostalgic feel the colors represent. 


## Reflection on object-oriented programming and object abstraction 

As mentioned in the introduction this week we worked with object abstraction and object-orientated approach. I think the abstraction of objects is evident in my game because of the use of vibrant cartoonish colors but also because of the unrealistic proportions and movements of my objects. I made flowers and corona-particles fall from the sky like rain in a dystopian imaginary world. In real life flowers and corona particles does not really fall from the sky in a literal sense. Of course, you may say, it is kind of true to reality when it comes to Corona, but you do not really see giant red mutations like objects fall from the sky in real life. The corona particles are too small for the human eye to notice their existence. Flowers can also fall to the sky, but it is also in more extreme or manipulated contexts if the weather is windy or they are physically thrown or dropped from another entity. 

To draw upon the assigned reading based on Matthew Fuller and Andrew Goffey, **“The Obscure Objects of Object Orientation,”** and Soon Winnie & Cox, Geoff, **"Object abstraction”**, object abstraction is about taking elements and properties from the natural environment of the real world and translating it into a form of digital narrative where the objects are still identifiable but have some new behaviors and characteristics that are not relatable to the real world. This abstraction of objects and environments opens for new narratives and alternative interpretations and experiences of the world. It creates a kind of limbo between real and abstracted entities. _“objects in OOP are not only about negotiating with the real world as a form of realism and representation, nor about the functions and logic that compose the objects, but the wider relations and “interactions between and with the computational.”_  (Soon and Cox, p. 145) It is important to understand that there are different layers to abstraction, not only is it about understanding the processes of computational qualities in relation to the concrete, but it is also about including and considering how the underlying politics and hidden meanings of abstraction and computational processes help model the world we live in. It is a transformative process and a way machines can talk and interpret the outside world.


## Cultural context

Working with abstraction as well as hidden computational layers of meanings really made me think about the world in a broader and more complex perspective. In my game I used two very contrasting elements, flowers and coronavirus. I think one of the reasons why I chose coronavirus as my enemy entity in my game is because it has common negative associations that I think many people can relate to. It is something many people are tired of dealing with and hopefully something people is trying to avoid “catching”. I chose flowers as my main “neutral” object because I think in general flowers have positive and liberating connotations. My game kind of displays a funny dynamic in the relation between the abstracted and the concrete. You are able to recognize the objects but in a new an alternative and computational manner which I think is the essence of abstraction and object-orientated approach. It is _“...a matter of creating models of things that don't otherwise exist.”_ (Fuller and Goffey, p. 5) 
In conclusion abstraction in computational relations creates a new era of interaction between humans and machines with both new possibilities and limitations of representation of objects in alternative and digital structures. 


## Inspiration 

https://editor.p5js.org/2sman/sketches/rkGp1alib  

https://editor.p5js.org/KevinWorkman/sketches/r17Ywmivm 

https://gitlab.com/aesthetic-programming/book/-/blob/master/public/p5_SampleCode/ch6_ObjectAbstraction/sketch.js 

https://gitlab.com/aesthetic-programming/book/-/tree/master/public/p5_SampleCode/ch6_ObjectAbstraction 

## References

Soon Winnie & Cox, Geoff, "Object abstraction", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 143-164

Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017). (on blackboard\Literature)

“p5.js examples - Array of Objects,” https://p5js.org/examples/objects-array-of-objects.html. 



