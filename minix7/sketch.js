// MiniX 7: object abstraction
// Amalie Sofie Due Jensen

let min_corona = 2;
let corona = [];

let min_flower = 3;
let flower = [];
let x = 300;
let button1;
let buttonState = false;
let buttonx;
let buttony;
let title;
let gameover;
let gamerules;

let winState = false;
let loseState = false;

let scoreMax = 15;
let loseMax = 10;
let score = 0, lose = 0;

let basketPosX;
let baskedPosY;
let basket = {
  w:110,
  h:60
} ;



function preload() {
  //load my different fonts
  title = loadFont('Corben-Bold.ttf');
  gameover = loadFont('Monofett-Regular.ttf');
  gamerules = loadFont('Corben-Regular.ttf');

  //load image of corona particle
  img = loadImage('coronavirus.png');
}




function setup() {
  createCanvas(windowWidth,windowHeight);
  // frameRate(5);

  //center the button
  buttonx = windowWidth/2;
  buttony = windowHeight/2;


  //create button, purple
  let col = color(204,153,255,190);
  // let col = color(138,43,226,170);
  button1 = createButton('Start Flower Power');
  button1.position(buttonx-140,buttony+200);
  button1.style('fontSize','25px');
  button1.style('font-family','Arial Black');
  button1.style('background-color', col);
  button1.mousePressed(changeBG);

//set basket size
  basketPosX = width/2;
  basketPosY = height-100

}

function changeBG() {
  //hide the button when pressed
  button1.hide();
  buttonState = true;
  winState = true;
  loseState = true;
}

function draw () {
  background(204,204,255);
  // background(153,204,255);
  checkFlowerNum();
  showFlower();
  checkEating();
  displayScore();
  checkWinResults();
  checkLoseResults();

  checkCoronaNum();
  showCorona();

//if the button is not pressed
if (!buttonState) {
  lose = 0
  //game rules box
  push();
  fill(255,250,240);
  stroke(204,153,255);
  strokeWeight(5);
  rectMode(CENTER);
  rect(windowWidth/2,290,500,350,8);
  pop();

  //game title: flower power
  push();
  fill(204,153,255);
  stroke(138,43,226);
  strokeWeight(5);
  textFont(title);
  textSize(50);
  textAlign(CENTER);
  text('Flower Power', windowWidth/2,80);
  pop();

  //Game Rules
  push();
  // fill(138,43,226);
  fill(0,204,102);
  // stroke(138,43,226);
  // strokeWeight(5);
  noStroke();
  textFont(title);
  textSize(30);
  textAlign(CENTER);
  text('Game Rules', windowWidth/2,200);
  pop();

  //text: Move Basket with ARROW LEFT and ARROW RIGHT
  push();
  fill(255,153,51);
  // stroke(138,43,226);
  // strokeWeight(5);
  stroke(255,153,51);
  textFont(gamerules);
  textSize(18);
  textAlign(CENTER);
  text('* Move Basket with ARROW LEFT and ARROW RIGHT', windowWidth/2,250);
  pop();

  //text: Get Points by catching flowers in the basket
  push();
  fill(255,153,51);
  // stroke(138,43,226);
  // strokeWeight(5);
    stroke(255,153,51);
  // noStroke();
  textFont(gamerules);
  textSize(20);
  textAlign(CENTER);
  text('* Get Points by catching flowers in the basket', windowWidth/2,290);
  pop();

  //Text:Avoid catching Corona-Virus'
  push();
  fill(255,153,51);
  // stroke(138,43,226);
  // strokeWeight(5);
  stroke(255,153,51);
  // noStroke();
  textFont(gamerules);
  textSize(20);
  textAlign(CENTER);
  text('* Avoid catching Corona-Virus (-1 point)', windowWidth/2,330);
  pop();

  //Text:Win the game by getting 15 points
  push();
  fill(255,153,51);
  // stroke(138,43,226);
  // strokeWeight(5);
  stroke(255,153,51);
  // noStroke();
  textFont(gamerules);
  textSize(20);
  textAlign(CENTER);
  text('* Win the game by cathing  15  flowers', windowWidth/2,370);
  pop();

// gamer over = 10 lost flowers
  push();
  fill(255,153,51);
  // stroke(138,43,226);
  // strokeWeight(5);
  stroke(255,153,51);
  // noStroke();
  textFont(gamerules);
  textSize(20);
  textAlign(CENTER);
  text('* Game Over =  10  lost flowers', windowWidth/2,410);
  pop();




//if the button is pressed/game starts
} else if (buttonState & winState & loseState) {

//Basket
  noFill();
  stroke(165,103,61);
  strokeWeight(8);
  ellipse(basketPosX+55,basketPosY,100,110);
  fill(192,119,70);
  stroke(165,103,61);
  strokeWeight(1);
  // rect(x,520,110,60,5);
    rect(basketPosX,basketPosY,basket.w,basket.h,5);


//make the basket move from left to right with LEFT_ARROW and RIGHT_ARROW
  if (keyIsDown(LEFT_ARROW)) {
    basketPosX -= 8
  }

  if (keyIsDown(RIGHT_ARROW)) {
    basketPosX += 8
  }
}
}


function displayScore() {
//Display the score board on top when the game starts
  if (buttonState & winState & loseState) {
    //score board on the top
    push();
    fill(255,250,240);
    stroke(204,153,255);
    strokeWeight(5);
    rectMode(CENTER);
    rect(windowWidth/2,0,600,150);
    pop();

    fill(138,43,226);
    noStroke();
    textSize(25);
    textFont(gamerules);
    text( + score +  " Collected Flowers", windowWidth/2-250,50);

    fill(138,43,226);
    noStroke();
    textSize(25);
    textFont(gamerules);
    text(+ lose + " lost Flowers", windowWidth/2+90,50);

  }


}

//make sure minimum amount of corona are displayed
function checkCoronaNum(){
  if (corona.length < min_corona){
    corona.push(new Corona());
  }
}
//show corona particles
function showCorona(){
  for (let i = 0; i < corona.length; i++){
    corona[i].move();
    corona[i].show();
  }
}

//make sure that minimum amount of flowers are displayed
function checkFlowerNum() {
  if (flower.length < min_flower) {
    flower.push(new Flower());
  }
}

//displaying flowers
function showFlower() {
  for (let i=0; i < flower.length; i++) {
    flower[i].move();
    flower[i].show();
  }
}

//making new flowers + corona from the top to keep the min_flower = 3/min_corona = 2
function checkEating() {

  //console.log(basketPosX)

  for(let i = 0; i < corona.length; i++){
    if(corona[i].pos.y > windowHeight){
      corona.splice(i,1);
    }
  }

  if(corona.length != null){
    for (let i = 0; i < corona.length; i++) {
      let d = int(
        dist(basketPosX + basket.w/2, basketPosY + basket.h-60,
          corona[i].pos.x, corona[i].pos.y)
        );

      //line(basketPosX + basket.w/2, basketPosY + basket.h-60, corona[i].pos.x+20, corona[i].pos.y+20)

      if (d < 50) {
        score--;
        corona.splice(i,1);
      }
    }
  }

  if(flower.length != null){
    for (let i = 0; i < flower.length; i++) {
      let d = int(
        dist(basketPosX + basket.w/2, basketPosY + basket.h-60,
          flower[i].pos.x, flower[i].pos.y)
        );

      //line(basketPosX + basket.w/2, basketPosY + basket.h-60, flower[i].pos.x, flower[i].pos.y)

      if (d < 50) {
        score++;
        flower.splice(i,1);
      }
      else if (flower[i].pos.y > windowHeight) {
        lose++;
        flower.splice(i,1);
      }
    }
  }


}

function checkWinResults() {

  //if the game is won
  if (score>=scoreMax) {
    winState = false;
    lose = 0;


    //gamle title
    push();
    fill(204,153,255);
    stroke(138,43,226);
    strokeWeight(5);
    textFont(title);
    textSize(50);
    textAlign(CENTER);
    text('Flower Power', windowWidth/2,80);
    pop();

    //Rule Box
    push();
    fill(255,250,240);
    stroke(204,153,255);
    strokeWeight(5);
    rectMode(CENTER);
    rect(windowWidth/2,290,500,350,8);
    pop();


    //You Win!
    push();
    // fill(138,43,226);
    fill(0,204,102);
    // stroke(138,43,226);
    // strokeWeight(5);
    noStroke();
    textFont(title);
    textSize(40);
    textAlign(CENTER);
    text('You Win!', windowWidth/2,250);
    pop();


    //text: Collected 15 flowers
    push();
    fill(255,153,51);
    // stroke(138,43,226);
    // strokeWeight(5);
    stroke(255,153,51);
    textFont(gamerules);
    textSize(22);
    textAlign(CENTER);
    text('Collected 15 flowers', windowWidth/2,300);
    pop();


  }
}

function checkLoseResults() {

  //if the game is lost
   if (lose>=loseMax) {
        loseState = false;

          //Game Title
            push();
            fill(204,153,255);
            stroke(138,43,226);
            strokeWeight(5);
            textFont(title);
            textSize(50);
            textAlign(CENTER);
            text('Flower Power', windowWidth/2,80);
            pop();

            push();
            fill(255,250,240);
            stroke(204,153,255);
            strokeWeight(5);
            rectMode(CENTER);
            rect(windowWidth/2,290,500,350,8);
            pop();


            //Game over!
            push();
            // fill(138,43,226);
            fill(0,random(150,250));
            // stroke(138,43,226);
            // strokeWeight(5);
            noStroke();
            textFont(gameover);
            textSize(60);
            textAlign(CENTER);
            text('Game Over!', windowWidth/2,300);
            pop();

  }
}
