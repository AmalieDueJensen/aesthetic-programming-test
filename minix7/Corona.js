// MiniX 7: object abstraction
// Amalie Sofie Due Jensen

class Corona {
  constructor() {
    this.speed = floor(random(2,5));
    this.size = 50;
    this.pos = new createVector(random(width), 0);
  }

  move() {
    this.pos.y += this.speed;
  }
  show() {
    image(img, this.pos.x, this.pos.y, this.size, this.size);
  }
}
