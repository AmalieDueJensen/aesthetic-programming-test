// MiniX 7: object abstraction
// Amalie Sofie Due Jensen


class Flower {
  constructor() {
    this.speed = floor(random(1,5));
    this.size = floor(random(15,25));
    this.pos = new createVector(random(width), 0);
  }

  move() {
    //make the flowers fall from the top of the canvas
    this.pos.y += this.speed;
  }

  show() {
    // Green flower leaf
    noStroke();
    fill(0,204,102);
      // fill(255,215,0);
         ellipse(this.pos.x - this.size/ 2, this.pos.y - this.size / 2, this.size);
              ellipse(this.pos.x + this.size/ 2, this.pos.y - this.size / 2, this.size);
                   ellipse(this.pos.x - this.size/ 2, this.pos.y + this.size / 2, this.size);
                        ellipse(this.pos.x + this.size/ 2, this.pos.y + this.size / 2, this.size);

        // orange flower center
         stroke(255);
         strokeWeight(1);
         fill(255,153,51);
         	// fill(51,0,25);
         // fill(random(255), random(255), random(255));
         ellipse(this.pos.x, this.pos.y, this.size);

}
}
