// MiniX 5: auto-generator
// Amalie Sofie Due Jensen


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  frameRate(30);
  }


function draw() {

  noStroke();
  let randomYlocation = random(windowHeight);

//make random sizes of ellipses (max 10 pixels) in random colors (pink/red/purple)
//only when y < 213
  if (randomYlocation < 213) {
    fill(random(230), random(50), random(100),random(20,230));
  }

  //make random sizes of ellipses (max 10 pixels) in random colors (green/orange/red)
  //draws ellipses on the y-axis between 203-456
  if(randomYlocation > 203 && randomYlocation < 456){
			fill(random(200), random(90), random(70),random(20,230));
	}

  //make random sizes of ellipses (max 10 pixels) in random colors (blue/green/purple)
  //when y > 466
	if(randomYlocation > 446){
		  fill(random(120), random(120), random(175),random(20,230));
  }

//set the ellipse size - max 10 pixels
  let randomdiameter = random(10);

//drawing/looping the ellipses in their desicnated areas on the y-axis
  for (let i = 0; i < 10 ; i++) {
    ellipse(random(windowWidth),randomYlocation, randomdiameter, randomdiameter+35);
  }

//draw loops of light purple ellipses on the entire canvas
  for (let j = 0; j < 1; j++) {
    fill(229,204,255,random(10,150));
    ellipse(random(windowWidth),random(windowHeight),randomdiameter,randomdiameter+35);
  }
  }
