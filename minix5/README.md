## Minix5 - Auto-Generator
Amalie Sofie Due Jensen

**Link to my program:** https://amalieduejensen.gitlab.io/aesthetic-programming-test/minix5/ 

**Link to code level:**  https://gitlab.com/AmalieDueJensen/aesthetic-programming-test/-/blob/master/minix5/sketch.js 


## The Rules

This week’s minix project with auto-generative art has been a really challenging project because I had a hard time figuring out what my rules should be and thereafter implementing them in a program. I knew I wanted to work with ellipses or just more organic shapes, to make a program with a more natural aesthetic.  
Eventually I chose the following rules. 

**1.	The ellipses should be drawn in random places and sizes throughout the canvas.** 

**2.	The ellipses should change colors in some areas ruled by coordinates from the x-axis or the y-axis.**


In the beginning I tried working with the 10-print source code just to get familiar with the syntax and explore the possibilities of the program. Here I changed colors and created an additional grid with an array of ellipses to connect the lines. Later I figured that I did not want to just copy the 10-print code in my program, so I started from scratch exploring generative options with ellipses. In the following section I will introduce my final program

![](minix5/10print.png)


## My Program

My final program consists exclusively of loops of small ellipses. The program starts with a black canvas and when the program starts the ellipses are drawn immediately little by little in different colors. My canvas is divided in three color-sections of ellipses on the y-axis. The top part is pink/red/purple, the middle is orange/red/green, and the bottom is mostly blue/purple/green. 
My rules produce emergent behavior because of the randomness of the placement of ellipses, level of transparency as well as the switching colors. I feel like my program looks like an abstract painting and it can therefore be interpreted in many ways. It kind of resembles rain because of the way the ellipses are shaped and the illusion of downward motion. This was not something I had thought of before making the program but discovered by modifying my code and trying out different things. 

![](minix5/start.png)

![](minix5/slut.png)


## The technical aspect  

The main syntax I have worked with in this minix is the _random()_ function. I used the random function to almost everything. I used it to make ellipses appear in random places and sizes (max 10 pixels, line 36) in random colors throughout the canvas. (line 32 + 40) I used _if statements_ to make the different colored ellipses appear in three horizontal sections on the y-axis. (line 19,25,31) Thereafter I used a _for loop_ to draw loops of ellipses continuously when the program is run. I also used the _alpha paramether_ to draw the ellipses in different levels of transparency. (line 32) It was really fun to work with a combination of these syntaxes and to see how small adjustment changed the entire aesthetic of the program. In the beginning I has made the ellipses larger, but I really liked the way the program looked when drawing smaller ellipses. I also chose to manipulate the original shape of the ellipses which was 10*10 pixels by adding +35 to the height. (line 40 + 46) This made the ellipses look like abstract raindrops or brushstrokes from a paintbrush.  
Because I had some difficulties while working with this project in the beginning, I chose to keep my program simple since it was what I could manage to create this week. I am always very ambitious with my programs and I am rarely satisfied with the outcome of the different projects and this project is not an exception. If I had more time this week I would have worked with more complex syntax and explored a more advanced generative program. This week’s work with auto-generative programs has therefore opened possibilities for improvement for future work.  


## Reflections on auto-generator

This minix really made me think of the importance of randomness in relation to generative art. Randomness has major influence in aesthetic but also technical relations. It gives projects an element of surprise and unpredictability. Because of the simplicity of my rules this program could have looked a lot different depending on aesthetic preferences and technical choices.  
It was really cool to work with the 10 prints source code, because 10 print can be regarded as the first creative program using machines to interpret instructions from humans resulting in a creative, random and generative maze pattern. 10 print introduced creative and artistic possibilities with algorithmic logic produced by mathematical machines. It was the beginning of generative art. 

_“Generative art refers to any art practice where artists use a system, such as a set of natural languages, rules, a computer program, a machine, or other procedural invention, which is set into motion with some degree of autonomy contributing to or resulting in a completed work of art.”_ 

(definition of generative art, Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 126)

It is interesting how randomness in programming can contribute to reinterpretation of the way we see and experience art. Generative art has become a platform to abstract art in similar ways we know it from classical art. Auto-generators creates new creative perspectives of art where machines can simulate living life and movements of complex organisms. This versatility in generative programming and inclusion of randomness and automation is what makes generative art so aesthetically powerful. 


## Inspiration

http://people.duke.edu/~acd41/assignments/final-project/final-project-instance-mode.html

https://p5js.org/reference/#/p5/for 

Soon Winnie & Cox, Geoff, "Auto-generator", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 121-142

